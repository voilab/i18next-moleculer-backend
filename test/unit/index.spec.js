'use strict';

const { ServiceBroker } = require('moleculer');
const { Backend, i18nMixin } = require('../../src');

describe('Test i18n mixin service constructor', () => {
    const broker = new ServiceBroker({ logger: false});
    const service = broker.createService(i18nMixin);

    it('should be created', () => {
        expect(service).toBeDefined();
        expect(service.i18next).toBeDefined();
    });
});

describe('Test i18n mixin service started handler', () => {
    const broker = new ServiceBroker({ logger: false});
    const service = broker.createService({
        name: 'i18nservice',
        mixins: [ i18nMixin ]
    });

    beforeAll(() => service._start());

    it('should call the i18n service', () => {
        expect(service).toBeDefined();
        expect(service.i18next).toBeDefined();
    });
});
