'use strict';

const getDefaults = () => {
    return {
        service: 'i18n',
        action: 'getLang',
        broker: null
    };
};

const NO_BROKER_ERROR = 'No broker instance provided to backend, unable to load translations';

class Backend {
    constructor (services, backendOptions = {}) {
        this.services = services;
        this.backendOptions = backendOptions;
        this.type = 'backend';
        this.init(services, backendOptions);
    }

    init (services, backendOptions = {}) {
        this.services = services;
        this.backendOptions = { ...getDefaults(), ...this.backendOptions, ...backendOptions };
        this.broker = this.backendOptions.broker;
    }

    read (language, namespace, callback) {
        if (!this.broker) {
            this.services.logger.error('moleculer-backend', NO_BROKER_ERROR);
            return callback(new Error(NO_BROKER_ERROR));
        }

        const action = this.backendOptions.action;

        this.services.logger.log('moleculer-backend', `fetching namespace "${namespace}" for language "${language}" from service "${action}"`);

        this.broker.call(action, { namespace, language })
            .then((response) => callback(null, response))
            .catch((err) => callback(err, false)); // no retry
    }
}

Backend.type = 'backend';

module.exports = Backend;
