'use strict';

const Backend = require('./backend');
const i18nMixin = require('./mixin');

module.exports = {
    Backend,
    i18nMixin,
};
