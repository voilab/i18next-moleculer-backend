'use strict';

const i18next = require('i18next');
const Backend = require('./backend');

module.exports = {
    name: 'i18next',

    settings: {
        /**
         * @type Object i18next settings
         * @see https://www.i18next.com/overview/configuration-options
         */
        i18next: {
            backend: {
                action: 'i18n.getLang'
            }
        },

        /**
         * @type String Name of the property containing the language inside `ctx.meta`.
         * @default 'lang'
         */
        i18nMetaPropName: 'lang',

        /**
         * @type String Name of the service and action providing the available languages config.
         * @default 'i18n.getLangsConfig'
         */
        i18nGetLngsAction: 'i18n.getLangsConfig'
    },

    methods: {
        /**
         * Get a translation fonction for the provided context.
         *
         * @param {Object} ctx Moleculer context for language auto detection
         *
         * @returns {Function} i18next.t()
         */
        async getT(ctx) {
            await this.i18nInit();

            const lng = ctx.meta[this.settings.i18nMetaPropName] || this.settings.i18next.fallbackLng;

            return this.i18next.getFixedT(lng);
        },

        async i18nInit() {
            if (this.i18nInitialized) {
                return;
            }
            const settings = Object.assign({}, this.settings.i18next);

            settings.backend = Object.assign({}, settings.backend);
            settings.backend.broker = this.broker;

            if (this.settings.i18nGetLngsAction) {
                const lngsConfig = await this.broker.call(this.settings.i18nGetLngsAction);

                if (lngsConfig.supportedLngs) {
                    settings.supportedLngs = lngsConfig.supportedLngs;
                }

                if (lngsConfig.fallbackLng) {
                    settings.fallbackLng = lngsConfig.fallbackLng;
                }
            }

            if (settings.supportedLngs && !settings.preload) {
                settings.preload = settings.supportedLngs;
            }

            if (settings.fallbackLng && !settings.lng) {
                settings.lng = settings.fallbackLng;
            }

            return this.i18next
                .use(Backend)
                .init(settings)
                .then(data => {
                    this.i18nInitialized = true;
                    return data;
                });
        }
    },

    events: {
        'i18n.namespace.changed.*': {
            params: {
                language: 'string',
                namespace: 'string'
            },

            handler(ctx) {
                if (!this.i18nInitialized) {
                    // to avoid errors if event occurs when i18next was not initialized
                    return;
                }
                const ns = this.settings.i18next.ns;

                if ((typeof ns === 'string' && ns !== ctx.params.namespace) ||
                    (Array.isArray(ns) && !ns.includes(ctx.params.namespace))) {
                    return;
                }

                this.logger.info(`Reloading language "${ctx.params.language}" for namespace "${ctx.params.namespace}"`);
                this.i18next.reloadResources(ctx.params.language, ctx.params.namespace);
            }
        }
    },

    created() {
        this.i18next = i18next.createInstance();
        this.i18nInitialized = false;
    }
};
